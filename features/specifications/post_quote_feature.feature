#language: pt
Funcionalidade: Quote
Como um usuário do sistema
Eu quero realizar as requisições na API Multicálculo
A fim de validar os cenários de uma cotação

@Quote
Cenário: Consumir o serviço POST Quote com retorno 200
    Dado que eu realize a autenticação com o usuario e senha para obter token
    Quando eu realizar uma requisição para criar uma cotação - Quote
    Então a API Multicálculo deve retornar os dados da requisição POST Quote com o código 200

@QuoteOrganizacaoSemPermissao
Cenário: Consumir o serviço POST Quote com retorno 500
    Dado que eu tenha configurado a organização para não permitir realizar cotações via API
    E eu verifique que o código do produtor contido na cotação está ligado a organização
    E eu realize a autenticação com o usuario e senha para obter token
    Quando eu realizar uma requisição para criar uma cotação - Quote organização sem permissão
    Então a API Multicálculo deve retornar a validação de permissão para cotar com o código 500

@QuoteComDadosOpcionais
Cenário: Consumir o serviço POST Quote com retorno 200
    Dado que eu realize a autenticação com o usuario e senha para obter token
    Quando eu realizar uma requisição para criar uma cotação com os dados opcionais preenchidos - Quote com dados opcionais
    Então a API Multicálculo deve retornar os dados da requisição POST Quote com o código 200

@QuoteNomeSobrenomeVazios
Cenário: Consumir o serviço POST Quote com retorno 500
    Dado que eu realize a autenticação com o usuario e senha para obter token
    E eu deixe a requisição quote com as tags firstname e lastname vazias
    Quando eu realizar uma requisição para criar uma cotação - Quote nome e sobrenome vazios
    Então a API Multicálculo deve retornar erro ao inserir a cotação com o código 500