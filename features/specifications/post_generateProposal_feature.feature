#language: pt
Funcionalidade: Generate Proposal
Como um usuário do sistema
Eu quero realizar as requisições na API Multicálculo
A fim de gerar uma proposta

@GenerateProposal
Cenário: Consumir o serviço POST Generate Proposal com retorno 200
    Dado que eu realize a autenticação com o usuario e senha para obter token
    E eu tenha criado anteriormente uma cotação
    E eu tenha gerado um plano de pagamento
    Quando eu realizar uma requisição para gerar uma proposta - Proposal
    Então a API Multicálculo deve retornar os dados da requisição POST Generate Proposal com o código 200