#language: pt
@CoveragesByOffer
Funcionalidade: Coverages By Offer
Como um usuário do sistema
Eu quero realizar as requisições na API Multicálculo
A fim de obter as coberturas das ofertas 

@CoveragesByOffer1
Esquema do Cenário: Consumir o serviço POST CoveragesByOffer com retorno 200. ofertas Exclusivo e Completo 
    Dado que eu realize a autenticação com o usuario e senha para obter token
    Quando eu realizar uma requisição para obter as coberturas das ofertas - CoveragesByOffer <oferta>
    Então a API Multicálculo deve retornar os dados da requisição POST CoveragesByOffer <oferta> com o código <codigo>

    Exemplos:
        |   oferta      |  codigo   |
        |   "Exclusivo" |    200    |
        |   "Completo"  |    200    |
  
@CoveragesByOffer2   
Esquema do Cenário: Consumir o serviço POST CoveragesByOffer com retorno 200, ofertas Compacto e Especial 
Dado que eu realize a autenticação com o usuario e senha para obter token
Quando eu realizar uma requisição para obter as coberturas das ofertas - CoveragesByOffer <oferta>
Então a API Multicálculo deve retornar os dados da requisição POST CoveragesByOffer <oferta> com o código <codigo>

    Exemplos:
        |   oferta      |  codigo   |
        |   "Compacto"  |    200    |
        |   "Especial"  |    200    |

@CoveragesByOffer3
Esquema do Cenário: Consumir o serviço POST CoveragesByOffer com retorno 200. ofertas Modelo e Essencial
Dado que eu realize a autenticação com o usuario e senha para obter token
Quando eu realizar uma requisição para obter as coberturas das ofertas - CoveragesByOffer <oferta>
Então a API Multicálculo deve retornar os dados da requisição POST CoveragesByOffer <oferta> com o código <codigo>

    Exemplos:
        |   oferta      |  codigo   |
        |   "Modelo"    |    200    |
        |   "Essencial" |    200    |
