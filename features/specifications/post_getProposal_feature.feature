#language: pt
Funcionalidade: Get Proposal
Como um usuário do sistema
Eu quero realizar a requisição na API Multicálculo
A fim de buscar os dados de uma proposta

@GetProposal
Cenário: Consumir o serviço POST Get Proposal com retorno 200
    Dado que eu realize a autenticação com o usuario e senha para obter token
    E eu tenha criado anteriormente uma cotação
    E eu tenha gerado um plano de pagamento
    E eu tenha gerado uma proposta
    Quando eu realizar uma requisição para buscar os dados da proposta - GetProposal
    Então a API Multicálculo deve retornar os dados da requisição POST Get Proposal com o código 200