#language: pt
Funcionalidade: Get Professions
Como um usuário do sistema 
Eu quero realizar as requisições na API Multicálculo
A fim de obter a lista de profissões

@GetProfessions
Cenário: Consumir o serviço POST Get Professions com retorno 200
    Dado que eu realize a autenticação com usuário e senha para obter token
    Quando eu realizar uma requisição para obter a lista de profissões
    Então a API Multicálculo deve retornar os dados da requisição POST Get Professions com código 200