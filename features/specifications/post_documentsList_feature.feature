#language: pt
Funcionalidade: Documents List
Como um usuário do sistema 
Eu quero realizar as requisições na API Multicálculo
A fim de obter a lista de documentos de uma proposta

@DocumentsList 
Cenário: Consumir o serviço POST Documents List com retorno 200
    Dado que eu realize a autenticação com usuário e senha para obter token
    E eu tenha criado uma cotação - Quote
    E eu tenha gerado um plano de pagamento - Generate Payment Plan
    E eu tenha gerado uma proposta - Generate Proposal
    Quando eu realizar uma requisição para obter a lista de documentos de uma proposta
    Então a API Multicálculo deve retornar os dados da requisição POST Documents List com código 200