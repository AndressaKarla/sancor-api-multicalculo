#language: pt
Funcionalidade: ECM Get Document
Como um usuário do sistema
Eu quero realizar a requisição na API Multicálculo
A fim de buscar um documento no serviço de integração ECM

@ECMGetDocument
Cenário: Consumir o serviço POST ECM Get Document com retorno 200
    Dado que eu realize a autenticação com o usuario e senha para obter token
    Quando eu realizar uma requisição para buscar o documento no serviço de integração ECM - ECMGetDocument
    Então a API Multicálculo deve retornar os dados da requisição POST ECM Get Document com o código 200