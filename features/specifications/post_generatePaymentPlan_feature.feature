#language: pt
Funcionalidade: Generate Payment Plan
Como um usuário do sistema
Eu quero realizar as requisições na API Multicálculo
A fim de gerar um plano de pagamento

@GeneratePaymentPlan
Cenário: Consumir o serviço POST Generate Payment Plan com retorno 200
    Dado que eu realize a autenticação com o usuario e senha para obter token
    E eu tenha criado anteriormente uma cotação
    Quando eu realizar uma requisição para gerar um plano de pagamento - Payment Plan
    Então a API Multicálculo deve retornar os dados da requisição POST Generate Payment Plan com o código 200