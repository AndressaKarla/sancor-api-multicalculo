class Quote
    include HTTParty
    require_relative '../hooks/quote_hook'

    # ambiente pre
    # def postQuote(body)
    #   @options = {
    #     headers: {
    #       "Authorization" => "Bearer #{$token}",
    #       "Content-Type" => "text/xml;charset=UTF-8"
    #     },
    #     body: body
    #   }
      
    #   return self.class.post("https://external-pre-ws.gruposancorseguros.com/Ssb/Policy/Services/Entities/PolicyAutoIntegrationService", @options)
    # end

    
    # ambiente devQA
    def postQuote(body)
      @options = {
        headers: {
          "Authorization" => "Bearer #{$token}",
          "Content-Type" => "text/xml;charset=UTF-8"
        },
        body: body
      }
      
      return self.class.post("https://iib-br-dev.gruposancorseguros.com/Ssb/Channel/PolicyAutoIntegrationService/devqa", @options)
    end
  end