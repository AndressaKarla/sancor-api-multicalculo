class GetProfessions
    include HTTParty
    require_relative '../hooks/getProfessions_hook'
    
    # ambiente pre
  #   def postGetProfessions(body)
  #     @options = {
  #       headers: {
  #         "Authorization" => "Bearer #{$token}",
  #         "Content-Type" => "text/xml;charset=UTF-8"
  #       },
  #       body: body
  #     }
      
  #     return self.class.post("https://external-pre-ws.gruposancorseguros.com/Ssb/Policy/Services/Entities/PolicyAutoIntegrationService", @options)
  #   end
  # end

  #ambiente devQA
    def postGetProfessions(body)
      @options = {
        headers: {
          "Content-Type" => "text/xml;charset=UTF-8"
        },
        body: body
      }
      
      return self.class.post("https://iib-br-dev.gruposancorseguros.com/Ssb/Channel/PolicyAutoIntegrationService/devqa", @options)
    end
  end