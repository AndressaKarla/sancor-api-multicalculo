class ECMGetDocument
    include HTTParty
    require_relative '../hooks/ecmGetDocument_hook'

    # ambiente pre
    # def postECMGetDocument(body)
    #   @options = {
    #     headers: {
    #       "Content-Type" => "text/xml;charset=UTF-8"
    #     },
    #     body: body
    #   }
      
    #   return self.class.post("", @options) link do ambiente pre desconhecido
    # end
    
    # ambiente devQA
    def postECMGetDocument(body)
      @options = {
        headers: {
          "Content-Type" => "text/xml;charset=UTF-8"
        },
        body: body
      }
      
      return self.class.post("https://iib-br-dev.gruposancorseguros.com/Ssb/Content/Services/Operations/ECMIntegrationServiceSync", @options)
    end
end