class GetProposal
    include HTTParty
    require_relative '../hooks/getProposal_hook'

    # ambiente pre
    # def postGetProposal(body)
    #   @options = {
    #     headers: {
    #       "Authorization" => "Bearer #{$token}",
    #       "Content-Type" => "text/xml;charset=UTF-8"
    #     },
    #     body: body
    #   }
      
    #   return self.class.post("https://external-pre-ws.gruposancorseguros.com/Ssb/Policy/Services/Entities/PolicyAutoIntegrationService", @options)
    # end
    
    # ambiente devQA
    def postGetProposal(body)
      @options = {
        headers: {
          "Content-Type" => "text/xml;charset=UTF-8",
          "LoginCorretor" => "434001"
        },
        body: body
      }
      
      return self.class.post("https://iib-br-dev.gruposancorseguros.com/Ssb/Channel/PolicyAutoIntegrationService/devqa", @options)
    end
end