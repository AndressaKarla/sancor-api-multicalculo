class Autenticacao
    include HTTParty
    require_relative '../hooks/autenticacao_hook'
  
    def initialize(body)
      @options = {
        headers: {
          "Auth0-Client" => "eyJuYW1lIjoibG9jay5qcyIsInZlcnNpb24iOiIxMC42LjEiLCJsaWJfdmVyc2lvbiI6IjcuNC4wIn0",
          "Content-Type" => "application/json"
        },
        body: body
      }
    end
  
    def doLogin()
      self.class.post("https://login-tst.gruposancorseguros.com/oauth/token", @options)
    end
    
end