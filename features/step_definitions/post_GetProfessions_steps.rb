require "rexml/document"
include REXML

Quando("eu realizar uma requisição para obter a lista de profissões") do
    file = File.new("./features/step_definitions/getProfessions.xml")  
    doc = Document.new(file)
   
    @body = %{#{doc}}

    $response = @getProfessions.postGetProfessions(@body)

  end
  
  Então("a API Multicálculo deve retornar os dados da requisição POST Get Professions com código {int}") do |statusCode|
    if $response.code == 200
        $response.each do |item|
           item.to_json                
        end 

        puts "\nResponse Code: #{$response.code}"

        i = 0 
        for  i in 0..70 do
            puts "\t\n#{i}: #{$response['Envelope']['Body']['getProfessionsResponse']['professions']['profession'][i]['description']}"
        end
    end
    expect($response.code).to eq(statusCode) 
end
