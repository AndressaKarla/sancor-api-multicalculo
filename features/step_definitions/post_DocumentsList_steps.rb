require "rexml/document"
include REXML

Dado("que eu realize a autenticação com usuário e senha para obter token") do
end

Dado("eu tenha criado uma cotação - Quote") do
end

Dado("eu tenha gerado um plano de pagamento - Generate Payment Plan") do
end

Dado("eu tenha gerado uma proposta - Generate Proposal") do
end

Quando("eu realizar uma requisição para obter a lista de documentos de uma proposta") do
    puts "\n"

    file = File.new("./features/step_definitions/documentsList.xml")  
    doc = Document.new(file)

    proposalNumber = "0000133819"

    XPath.each(doc, "//proposalNumber") do |node|
        node.text = proposalNumber
    end

    doc.write(File.open("./features/step_definitions/documentsList.xml","w"))

    @body = %{#{doc}}

    $response = @documentsList.postDocumentsList(@body)

    puts "\n\n"

    # puts @body
    # puts "\n\n"
end

Então("a API Multicálculo deve retornar os dados da requisição POST Documents List com código {int}") do |statusCode|
    if $response.code == 200
        $response.each do |item|
           item.to_json                
        end 
    
        puts "\nResponse Code: #{$response.code}"
        puts "ID do Arquivo: #{$response['Envelope']['Body']['getDocumentsListResponse']['documents']['document']['fileID']}"
        puts "Nome do Arquivo: #{$response['Envelope']['Body']['getDocumentsListResponse']['documents']['document']['name']}"
        puts "Tipo do Arquivo: #{$response['Envelope']['Body']['getDocumentsListResponse']['documents']['document']['mimeType']}"
    end
    
    expect($response.code).to eq(statusCode) 
end