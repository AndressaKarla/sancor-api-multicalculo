require "rexml/document"  
include REXML 

file = File.new("./features/step_definitions/quote.xml")  
$doc = Document.new(file)

Dado("que eu tenha configurado a organização para não permitir realizar cotações via API") do
    
end
  
Dado("eu verifique que o código do produtor contido na cotação está ligado a organização") do
    
end

Dado("eu deixe a requisição quote com as tags firstname e lastname vazias") do
    
end
  
Dado("eu realize a autenticação com o usuario e senha para obter token") do
    
end

Quando("eu realizar uma requisição para criar uma cotação - Quote") do 
    @body = %{#{$doc}}

    $response = @quote.postQuote(@body)
end

Quando("eu realizar uma requisição para criar uma cotação - Quote organização sem permissão") do   
    codePolicyVendor = "100635782"
    XPath.each($doc, "//code") do |node|
        node.text = codePolicyVendor
    end

    $doc.write(File.open("./features/step_definitions/quote.xml","w"))

    @body = %{#{$doc}}
    $response = @quote.postQuote(@body)
end

#@QuoteComDadosOpcionais
Quando("eu realizar uma requisição para criar uma cotação com os dados opcionais preenchidos - Quote com dados opcionais") do
    file = File.new("./features/step_definitions/quoteComDadosOpcionais.xml")  
    doc = Document.new(file)
   
    @body = %{#{doc}}

    $response = @quote.postQuote(@body)
end

Quando("eu realizar uma requisição para criar uma cotação - Quote nome e sobrenome vazios") do
    XPath.each($doc, "//v13:firstName") do |node|
        node.text = ""
    end    

    XPath.each($doc, "//v13:lastName") do |node|
        node.text = ""
    end

    $doc.write(File.open("./features/step_definitions/quote.xml","w")) 

    @body = %{#{$doc}}
    $response = @quote.postQuote(@body)
end

Então("a API Multicálculo deve retornar os dados da requisição POST Quote com o código {int}") do |statusCodePost|
    if $response.code == 200
        $response.each do |item|
           item.to_json                
        end 

        puts "\nResponse Code: #{$response.code}"
        puts "Nº da Cotação: #{$response['Envelope']['Body']['getQuoteResponse']['quoteResponse']['quoteID']}"
        puts "Veículo: #{$response['Envelope']['Body']['getQuoteResponse']['quoteResponse']['quotedItems']['item']['vehicleName']}"
        puts "Coberturas: "

        i = 0
        for i in 0..10 do
            puts "\t#{i}: #{$response['Envelope']['Body']['getQuoteResponse']['quoteResponse']['quotedItems']['item']['coverages']['vehicleCoverage'][i]['name']}"
        end
    end

    expect($response.code).to eq(statusCodePost)
end
  
Então("a API Multicálculo deve retornar a validação de permissão para cotar com o código {int}") do |statusCode|
    if $response.code == 500
        $response.each do |item|
           item.to_json
        end

        puts "\nResponse Code: #{$response.code}"
        
        getFault = $response['Envelope']['Body']['Fault']

        fault = { 'faultcode' => 'Client', 'faultstring' => 'Corretora não habilitada para cotações via API.' }
        
        fault.each do |key, value|
            expect(getFault[key]).to eq(fault[key])
            puts "\n#{key}: #{getFault[key]}"
        end

        expect($response.code).to eq(statusCode)
    end

    codePolicyVendor = "434001"
    XPath.each($doc, "//code") do |node|
        node.text = codePolicyVendor
    end

    $doc.write(File.open("./features/step_definitions/quote.xml","w"))   
end

Então("a API Multicálculo deve retornar erro ao inserir a cotação com o código {int}") do |statusCode|
    if $response.code == 500
        $response.each do |item|
           item.to_json
        end

        puts "\nResponse Code: #{$response.code}"
        
        getFault = $response['Envelope']['Body']['Fault']

        fault = { 'faultcode' => 'Client.Validation', 'faultstring' => 'A schema validation error has occurred while parsing the XML document' }
        
        fault.each do |key, value|
            expect(getFault[key]).to eq(fault[key])
            puts "\n#{key}: #{getFault[key]}"
        end  
    end

    XPath.each($doc, "//v13:firstName") do |node|
        node.text = "TESTE"
    end    

    XPath.each($doc, "//v13:lastName") do |node|
        node.text = "MULTICALCULO API AUTOMATIZADO"
    end

    $doc.write(File.open("./features/step_definitions/quote.xml","w")) 

    expect($response.code).to eq(statusCode)   
end