require "rexml/document"  

include REXML 

Quando("eu realizar uma requisição para gerar um plano de pagamento - Payment Plan") do
    file = File.new("./features/step_definitions/generatePaymentPlan.xml")  
    doc = Document.new(file)
   
    @body = %{#{doc}}

    # puts @body

    $response = @generatePaymentPlan.postGeneratePaymentPlan(@body)
end

Então("a API Multicálculo deve retornar os dados da requisição POST Generate Payment Plan com o código {int}") do |statusCodePost|
    if $response.code == 200
        $response.each do |item|
           item.to_json                
        end 

        puts "\nResponse Code: #{$response.code}"
        puts "Método de Pagamento: #{$response['Envelope']['Body']['generatePaymentPlanResponse']['paymentPlans']['paymentPlan'][0]['paymentMethod']}"
        puts "Planos de Pagamento: "

        i = 0
        for i in 0..9 do
            puts "\t#{i}: #{$response['Envelope']['Body']['generatePaymentPlanResponse']['paymentPlans']['paymentPlan'][i]['name']}"
            puts "\tValor: R$ #{$response['Envelope']['Body']['generatePaymentPlanResponse']['paymentPlans']['paymentPlan'][i]['downPayment']['amount']}"
            puts "\n"
        end
    end

    expect($response.code).to eq(statusCodePost)
end