require "rexml/document"  

include REXML 

Dado("eu tenha criado anteriormente uma cotação") do

end

Dado("eu tenha gerado um plano de pagamento") do

end

Quando("eu realizar uma requisição para gerar uma proposta - Proposal") do
    file = File.new("./features/step_definitions/generateProposal.xml")  
    doc = Document.new(file)
   
    @body = %{#{doc}}

    # puts @body

    $response = @generateProposal.postGenerateProposal(@body)
end

Então("a API Multicálculo deve retornar os dados da requisição POST Generate Proposal com o código {int}") do |statusCodePost|
    if $response.code == 200
        $response.each do |item|
           item.to_json                
        end 

        puts "\nResponse Code: #{$response.code}"
        puts "Nº da Proposta: #{$response['Envelope']['Body']['generateProposalResponse']['proposalResult']['proposalNumber']}"
        puts "Plano de Pagamento: #{$response['Envelope']['Body']['generateProposalResponse']['proposalResult']['paymentPlan']['name']}"
        puts "Valor Total: R$ #{$response['Envelope']['Body']['generateProposalResponse']['proposalResult']['paymentPlan']['total']['amount']}"
        puts "Veículo: #{$response['Envelope']['Body']['generateProposalResponse']['proposalResult']['coverables']['item']['vehicleName']}"
        puts "Coberturas: "

        i = 0
        for i in 0..10 do
            puts "\t#{i}: #{$response['Envelope']['Body']['generateProposalResponse']['proposalResult']['coverables']['item']['coverages']['vehicleCoverage'][i]['name']}"
        end
    end

    expect($response.code).to eq(statusCodePost)
end