require "rexml/document"  

include REXML 

Dado("que eu realize a autenticação com o usuario e senha para obter token") do

end

Quando("eu realizar uma requisição para obter as coberturas das ofertas - CoveragesByOffer {string}") do |oferta|
    puts "\n"

    file = File.new("./features/step_definitions/coveragesByOffer.xml")  
    doc = Document.new(file)

    XPath.each(doc, "//name") do |node|
        node.text = oferta
    end

    doc.write(File.open("./features/step_definitions/coveragesByOffer.xml","w"))

    @body = %{#{doc}}

    $response = @coveragesByOffer.postCoveragesByOffer(@body)

    puts "\n\n"

    # puts @body
    # puts "\n\n"
end

Então("a API Multicálculo deve retornar os dados da requisição POST CoveragesByOffer {string} com o código {int}") do |oferta, statusCodePost|
    if $response.code == 200
        $response.each do |item|
             item.to_json                
        end 

        puts "\nResponse Code: #{$response.code}"
        puts "Coberturas:"
        

        i = 0
        if oferta == "Exclusivo" || oferta == "Completo"
            for i in 0..24 do
                puts "\t\n#{i}: #{$response['Envelope']['Body']['getVehicleCoveragesByOfferResponse']['vehicleCoverages']['vehicleCoverage'][i]['name']}"
            end
        end

        if oferta == "Compacto"
            for i in 0..19 do
                puts "\t\n#{i}: #{$response['Envelope']['Body']['getVehicleCoveragesByOfferResponse']['vehicleCoverages']['vehicleCoverage'][i]['name']}"
            end
        end

        if oferta == "Especial"
            for i in 0..20 do
                puts "\t\n#{i}: #{$response['Envelope']['Body']['getVehicleCoveragesByOfferResponse']['vehicleCoverages']['vehicleCoverage'][i]['name']}"
            end
        end

        if oferta == "Modelo"
            for i in 0..22 do
                puts "\t\n#{i}: #{$response['Envelope']['Body']['getVehicleCoveragesByOfferResponse']['vehicleCoverages']['vehicleCoverage'][i]['name']}"
            end
        end

        if oferta == "Essencial"
            for i in 0..8 do
                puts "\t\n#{i}: #{$response['Envelope']['Body']['getVehicleCoveragesByOfferResponse']['vehicleCoverages']['vehicleCoverage'][i]['name']}"
            end
        end
    end

    expect($response.code).to eq(statusCodePost)
end