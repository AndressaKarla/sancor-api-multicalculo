$proposalNumber = "0000133857"

Quando("eu realizar uma requisição para buscar os dados da proposta - GetProposal") do
    file = File.new("./features/step_definitions/getProposal.xml")  
    doc = Document.new(file)
    
    XPath.each(doc, "//proposalNumber") do |node|
        node.text = $proposalNumber
    end

    doc.write(File.open("./features/step_definitions/getProposal.xml","w"))
    @body = %{#{doc}}
    $response = @getProposal.postGetProposal(@body)
end
  
Então("a API Multicálculo deve retornar os dados da requisição POST Get Proposal com o código {int}") do |statusCode|
    if $response.code == 200
        $response.each do |item|
           item.to_json                
        end 

        getProposalNumber = $response['Envelope']['Body']['getProposalResponse']['proposalResponse']['proposalNumber']

        puts "\nResponse Code: #{$response.code}"
        puts "\nProposal Number: #{getProposalNumber}"
    end
    
    expect($response.code).to eq(statusCode) 
    expect(getProposalNumber).to eq($proposalNumber)
end