require "rexml/document"
include REXML

Dado("eu tenha gerado uma proposta") do
    
end
  
Quando("eu realizar uma requisição para buscar os boletos bancários por proposta - GetBankSlipsByProposal") do
    file = File.new("./features/step_definitions/getBankSlipsByProposal.xml")  
    doc = Document.new(file)
    
    proposalNumber = "0000133853"
    XPath.each(doc, "//proposalNumber") do |node|
        node.text = proposalNumber
    end

    doc.write(File.open("./features/step_definitions/getBankSlipsByProposal.xml","w"))

    @body = %{#{doc}}

    $response = @getBankSlipsByProposal.postGetBankSlipsByProposal(@body)
end
  
Então("a API Multicálculo deve retornar os dados da requisição POST Get Bank Slips By Proposal com o código {int}") do |statusCode|
    if $response.code == 200
        $response.each do |item|
           item.to_json                
        end 

        puts "\nResponse Code: #{$response.code}"
        
        getBeneficiary = $response['Envelope']['Body']['getBankSlipsByProposalResponse']['bankSlips']['bankSlip']['beneficiary']

        beneficiary = { 'convenienceCode' => '0', 'name' => 'SANCOR SEGUROS DO BRASIL S.A', 'cpfCnpj' => '17643407000130', 'bankCode' => '756', 
                        'bankName' => 'SICOOB', 'agencyNumber' => '4340', 'agencyCheckDigit' => '0', 'accountNumber' => '119381', 'checkDigitAccount'=> '3' }
        
        beneficiary.each do |key, value|
            expect(getBeneficiary[key]).to eq(beneficiary[key])
            puts "\n#{key}: #{getBeneficiary[key]}" 
        end

    end
    expect($response.code).to eq(statusCode) 
end