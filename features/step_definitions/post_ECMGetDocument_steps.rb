Quando("eu realizar uma requisição para buscar o documento no serviço de integração ECM - ECMGetDocument") do
    file = File.new("./features/step_definitions/ecmGetDocument.xml")  
    doc = Document.new(file)
    
    fileID = "pc:79508_test"
    XPath.each(doc, "//FileId") do |node|
        node.text = fileID
    end

    doc.write(File.open("./features/step_definitions/ecmGetDocument.xml","w"))

    @body = %{#{doc}}

    $response = @ecmGetDocument.postECMGetDocument(@body)
end
  
Então("a API Multicálculo deve retornar os dados da requisição POST ECM Get Document com o código {int}") do |statusCode|
    if $response.code == 200
        $response.each do |item|
           item.to_json                
        end 

        puts "\nResponse Code: #{$response.code}"
        
        getStream = $response['Envelope']['Body']['GetDocumentResponse']['GetFileResponse']['Stream']
    end
    expect($response.code).to eq(statusCode)
    expect(getStream).not_to be_empty
end